import React, { Component } from 'react'
import { Provider } from 'react-redux'
import AppNavigator from './src/navigation/AppNavigator'
import { Censtral_Store } from './src/redux/store/Central_Store'


export class App extends Component {
  render() {
    return (
      <Provider store={Censtral_Store}>
        <AppNavigator></AppNavigator>
     </Provider>
    )
  }
}

export default App
