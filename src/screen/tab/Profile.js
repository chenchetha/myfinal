import { Body, Button, Card, Icon, Left, ListItem, Right, Thumbnail } from 'native-base'
import React, { Component } from 'react'
import { ImageBackground, SafeAreaView, Text, TouchableOpacity, View } from 'react-native'
import { LoginManager } from 'react-native-fbsdk'
import { connect } from 'react-redux'
import { color } from '../../theme/Color'
import { font } from '../../theme/Font'

export class Profile extends Component {

    state = {
        label: 'Profile'
    }

    logOut=()=>{
        LoginManager.logOut();
        this.props.navigation.navigate('Login');
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <View>
                    <ImageBackground style={{ width: "100%", height: 55 }} source={require('../../images/my_back.png')}>
                        <View style={{ flex: 1, alignItems: "flex-end", marginRight: 15, marginTop: 5 }}>
                            <Text style={{ fontFamily: "Billabong", fontSize: 45 }}>{this.state.label}</Text>
                        </View>
                    </ImageBackground>
                </View>
                <Card style={{ marginLeft: 15, marginRight: 15, height: 150, marginTop: 10,borderRadius:10, padding: 20, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity activeOpacity={1} >
                        <Thumbnail large source={{ uri: this.props.article.user.pic.url }} style={{ margin: 2 }} />
                    </TouchableOpacity>
                    <View >
                        <Text style={{ color: color.danger, fontSize: font.size_header, textAlign: "center", fontWeight: "bold", marginTop: 5 }}>
                            {this.props.article.user.user.name}
                        </Text>
                    </View>
                </Card>
                <View style={{ margin: 15,}}>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: color.danger }}>
                                <Icon active type="MaterialIcons" name="more" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>See More</Text>
                        </Body>
                        <Right>
                            <Icon active name="arrow-forward" />
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: color.danger }}>
                                <Icon active type="MaterialIcons" name="help" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Help & Support </Text>
                        </Body>
                        <Right>
                            <Icon active name="arrow-forward" />
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: color.danger }}>
                                <Icon active type="MaterialIcons" name="settings" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Setting & Privacy </Text>
                        </Body>
                        <Right>
                            <Icon active name="arrow-forward" />
                        </Right>
                    </ListItem>
                    <ListItem icon onPress={()=>this.logOut()}>
                        <Left>
                            <Button style={{ backgroundColor: color.danger }}>
                                <Icon active type="MaterialIcons" name="exit-to-app" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Log Out </Text>
                        </Body>
                        <Right>
                            <Icon active name="arrow-forward" />
                        </Right>
                    </ListItem>
                </View>

            </SafeAreaView>
        )
    }
}



const mTp = (store) => {
    return {
        article: store.articleR
    }
}

export default connect(mTp,)(Profile)