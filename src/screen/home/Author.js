import { Body, Button, Card, Icon, Left, ListItem, Right, Switch, Thumbnail } from 'native-base'
import React, { Component } from 'react'
import { SafeAreaView, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { color } from '../../theme/Color'
import { font } from '../../theme/Font'
import { style_in } from '../authentication/Login'

export class Author extends Component {
    render() {
        const { item } = this.props.route.params
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ width: "100%", height: 300, flex: 1, backgroundColor: color.danger }}>

                </View>
                <View style={{ flex: 3 }}>
                    <Card style={{ marginLeft: 15, marginRight: 15, height: 200, marginTop: -150, borderTopLeftRadius: 25, borderTopRightRadius: 25, padding: 20, justifyContent: "center", alignItems: "center" }}>
                        <TouchableOpacity activeOpacity={1} >
                            <Thumbnail large source={{ uri: item && item.image }} style={{ margin: 2 }} />
                        </TouchableOpacity>
                        <View >
                            <Text style={{ color: color.danger, fontSize: font.size_header, textAlign: "center", fontWeight: "bold", marginTop: 5 }}>
                                {item.name}
                            </Text>
                        </View>
                        <View style={{ flexDirection: "row", marginTop: 5 }}>
                            <View>
                                <Text>Email : </Text>
                            </View>
                            <View>
                                <Text style={{ color: "gray", fontSize: font.size_title, textAlign: "center" }}>
                                    {item.email}
                                </Text>
                            </View>
                        </View>
                    </Card>
                    <View>
                        <Card style={{ marginLeft: 15, marginRight: 15, marginTop: 5, borderBottomLeftRadius: 25, borderBottomRightRadius: 25, padding: 10 }}>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: color.danger }}>
                                        <Icon active type="MaterialIcons" name="business-center" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>My article</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" />
                                </Right>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: color.danger }}>
                                        <Icon active type="MaterialIcons" name="contact-mail" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Contact me </Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" />
                                </Right>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: color.danger }}>
                                        <Icon active type="MaterialIcons" name="share" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Share with friends </Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" />
                                </Right>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: color.danger }}>
                                        <Icon active type="MaterialIcons" name="remove-red-eye" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Review </Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" />
                                </Right>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: color.danger }}>
                                        <Icon active type="MaterialIcons" name="info" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Info </Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" />
                                </Right>
                            </ListItem>
                            <ListItem icon noBorder>
                                <Left>
                                    <Button style={{ backgroundColor: color.danger }}>
                                        <Icon active type="MaterialIcons" name="favorite" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Your Favorite </Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" />
                                </Right>
                            </ListItem>
                        </Card>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

export default Author
