import { ActionSheet, Body, Button, Card, CardItem, Container, Content, Icon, Input, Item, Root, Thumbnail } from 'native-base';
import React, { Component } from 'react'
import { ActivityIndicator, Alert, FlatList, Image, ImageBackground, RefreshControl, SafeAreaView, Text, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux';
import DailogWaiting from '../../components/DailogWaiting';
import { convertDate } from '../../function/function';
import { style_nav } from '../../navigation/AppNavigator';
import { GetArticle, GetAuthor, DeleteArticle } from '../../redux/action/Article_Action'
import { color } from '../../theme/Color';
import { font } from '../../theme/Font';

var BUTTONS = [
    { text: "Edit", icon: "pencil", iconColor: color.danger },
    { text: "Delete", icon: "trash", iconColor: color.danger },
    { text: "Cancel", icon: "backspace", iconColor: color.danger }
];
var DESTRUCTIVE_INDEX = 1;
var CANCEL_INDEX = 2;

export class Home extends Component {

    state = {
        articles: [],
        authors: [],
        islove: false,
        index: '',
        lastPress: 0,
        query: {
            page: 1,
            limit: 10,
            search: ''
        },
        isLoading: false,
        refreshing: false,
        isRefresh: false,
        isLoadingFooter: false,
        totalPage: 0,
        image : 'https://www.kinmade.com/pub/media/catalog/product/placeholder/default/no-image-available_1.jpg'
    }



    onRefresh =  () => {
        this.setState(prevState => ({
            ...prevState,
            isRefresh: true,
            query: {
                page: 1,
                limit: 10,
                search: ''
            }
        }))
    }


    onClickLove = (index) => {
        this.setState({
            islove: true,
            index: index
        })
    }

    onMore = () => {
        if (this.state.query.page <= this.state.totalPage) {
            this.setState(prevState => ({
                ...prevState,
                query: {
                    ...prevState.query,
                    page: this.state.query.page + 1
                }
            }))
        }
    }


    renderFooter = () => {
        return (
            this.state.isLoadingFooter ?
                <View>
                    <ActivityIndicator size="large" color={color.danger} style={{ margin: 10 }} />
                </View>
                :
                null
        )
    }


    componentDidUpdate = async (prevProp, prevState) => {

        if (prevState.islove !== this.state.islove && this.state.islove === true) {

            let index = this.state.index;
            this.state.articles[index].__v = this.state.articles[index].__v === 1 ? 0 : 1;
            this.setState({ islove: false });

        }

        if (this.state.isRefresh === true && (prevState.isRefresh !== this.state.isRefresh) ) {
          
            await this.props.GetArticle(this.state.query);
            await this.props.GetAuthor();

            this.setState(prevState => ({
                ...prevState,
                isRefresh: false,
                articles: this.props.article.lst_article.data,
                authors: this.props.article.lst_author.data,
            }))

         
        }

        if ((this.state.query.page !== prevState.query.page) && (this.state.query.page !== 1)) {

            this.setState({
                isLoadingFooter: true
            })

            await this.props.GetArticle(this.state.query);
            this.setState(prevState => ({
                ...prevState,
                articles: prevState.articles.concat(this.props.article.lst_article.data),
                isLoadingFooter: false
            }))
        }


        if (this.state.query.search !== prevState.query.search) {

            await this.props.GetArticle(this.state.query);
            this.setState(prevState => ({
                ...prevState,
                articles: this.props.article.lst_article.data,
            }))
        }

        if (prevProp.article.isUpdate !== this.props.article.isUpdate && this.props.article.isUpdate === true) {
            this.setState({
                isRefresh: true
            })
            this.props.article.isUpdate = false
        }

    }


    componentDidMount = async () => {


        this.setState({
            isLoading: true,
        })

        await this.props.GetArticle(this.state.query);
        await this.props.GetAuthor();

        this.setState({
            articles: this.props.article.lst_article.data,
            authors: this.props.article.lst_author.data,
            isLoading: false,
            totalPage: this.props.article.lst_article.total_page
        })
    }



    onClickImg = (item) => {
        this.props.navigation.navigate('Home_Detail', { item: item });
    }

    onCallOption = (item) => {
        ActionSheet.show(
            {
                options: BUTTONS,
                cancelButtonIndex: CANCEL_INDEX,
                destructiveButtonIndex: DESTRUCTIVE_INDEX,
                title: "Option"
            },
            buttonIndex => {
                if (buttonIndex === 0) {
                    this.props.navigation.navigate('Home_Add', { item: item });
                } else if (buttonIndex === 1) {
                    Alert.alert(
                        "Confirm",
                        "Are you sure want to delete ?",
                        [
                            {
                                text: "Cancel",
                                onPress: () => console.log("Cancel Pressed"),
                                style: "cancel"
                            },
                            { text: "OK", onPress: () => this.onDelete(item._id) }
                        ],
                        { cancelable: false }
                    );
                }
            }
        )
    }

    onDelete = async (id) => {

        this.setState({
            isLoading: true,           
        })
        await this.props.DeleteArticle(id)
        this.onRefresh();

        this.setState({
            isLoading: false,
        })
    }

    keyExtractor = (item, index) => index.toString()
    renderItem = ({ item, index }) => (
       
            <Card style={{ marginLeft: 10, marginRight: 10, borderLeftWidth: 5, borderColor: color.danger, borderRadius: 5 }} >
                <View style={{ flexDirection: "row" }}>
                    <View style={{ margin: 10, width: 25, height: 25, borderRadius: 10, backgroundColor: color.danger, justifyContent: "center", alignItems: "center" }} >
                        <TouchableOpacity activeOpacity={0.8} >
                            <Thumbnail source={{ uri: item.author && item.author.image }} style={{ width: 25, height: 25, borderRadius: 10 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ justifyContent: "center", alignItems: "center", marginBottom: 10 }}>
                        <Text style={{ fontWeight: "bold" }}>{item.author == null ? "No Author" : item.author.name} </Text>
                    </View>
                    <View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
                        <TouchableOpacity onPress={() => this.onCallOption(item)}>
                            <Icon type="MaterialIcons" name="more-vert" style={{ color: color.danger, margin: 20, fontSize: font.size_header }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View>
                    <TouchableOpacity activeOpacity={0.9} onPress={() => this.onClickImg(item)}>
                        <ImageBackground source={{ uri: item.image === '' ? this.state.image : item.image }} style={{ height: 200, width: '100%' ,resizeMode: 'contain' }} >
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
                <View style={{ margin: 5}}>
                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1 }}>
                            <Text numberOfLines={2} style={{ color: color.danger, fontSize: font.size_header, fontWeight: "bold" }}>{item.title}</Text>
                        </View>
                        <View style={{ alignItems: "flex-end", flex: 1 }}>
                            <Text style={{ fontSize: font.size_title, color: "gray" }}>{convertDate(item.createdAt)}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row",marginBottom:5 ,marginTop: 5 }}>
                        <View style={{ flexDirection: "row" }}>
                            <Text style={{ fontSize: font.size_title }}>Category :{' '}</Text>
                            <Text style={{ fontSize: font.size_title }}>{item.category && item.category.name}</Text>
                        </View>
                        <View style={{ alignItems: "flex-end", flex: 1, marginRight: 10, }}>
                            <View style={{ flexDirection: "row" }}>
                                <TouchableOpacity onPress={() => this.onClickLove(index)} style={{ marginRight: 10 }}>
                                    <Icon type="MaterialIcons" name={item.__v === 0 ? "favorite-border" : "favorite"} style={{ color: item.__v === 1 ? color.danger : "black" }} />
                                </TouchableOpacity>
                                <TouchableOpacity >
                                    <Icon type="MaterialIcons" name="share" />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Card>
           
    )

    keyExtractorAuthor = (item, index) => index.toString()
    renderItemAuthor = ({ item, index }) => (
        <View style={{ justifyContent: "center", alignItems: "center", margin: 5, width: 125 }}>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
                <Card style={{ width: 60, height: 60, borderRadius: 60, backgroundColor: color.danger, padding: 2, justifyContent: "center", alignItems: "center" }} >
                    <TouchableOpacity activeOpacity={0.8} onPress={() =>  this.props.navigation.navigate('Author', { item: item })}>
                        <Thumbnail source={{ uri: item && item.image }} style={{ margin: 2 }} />
                    </TouchableOpacity>
                </Card>
            </View>
            <View >
                <Text style={{ textAlign: "center" }}>{item.name}</Text>
            </View>
        </View>
    )



    render() {


        return (
            <Root>
                <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                    <DailogWaiting visible={this.state.isLoading}> </DailogWaiting>
                    <ImageBackground style={{ width: "100%", height: 55 }} source={require('../../images/my_back.png')}>
                        <View style={{ flex: 1, alignItems: "flex-end", marginRight: 15, marginTop: 5 }}>
                            <Text style={{ fontFamily: "Billabong", fontSize: 45 }}>Article</Text>
                        </View>
                    </ImageBackground>

                    <View>
                        <FlatList
                            keyExtractor={this.keyExtractorAuthor}
                            data={this.state.authors}
                            renderItem={this.renderItemAuthor}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            style={{ margin: 5 }}
                            onRefresh={this.onRefresh}
                            refreshing={this.state.refreshing}
                        />
                    </View>

                    <View style={{ flexDirection: "row", margin: 8 }}>
                        <View style={{ flex: 7, marginRight: 10 }}>
                            <Item regular style={{ borderRadius: 5 }}>
                                <Icon active name='search' style={{ color: color.danger }} />
                                <Input placeholder='Search article' value={this.state.query.search} onChangeText={(text) => this.setState({ query: { search: text } })} />
                            </Item>
                        </View>
                        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Home_Add', { item: null })} style={{ backgroundColor: color.danger, height: 50, width: 50, borderRadius: 5, justifyContent: "center", alignItems: "center" }}>
                                <Icon active name='add' style={{ color: color.white }} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <FlatList
                        keyExtractor={this.keyExtractor}
                        data={this.state.articles}
                        renderItem={this.renderItem}
                        refreshControl={
                            <RefreshControl
                                colors={[color.danger, color.danger]}
                                onRefresh={this.onRefresh}
                                refreshing={this.state.refreshing}
                            />
                        }
                        ListFooterComponent={this.renderFooter}
                        onEndReached={this.onMore}
                    />
                </SafeAreaView>
            </Root>
        )
    }
}
const mTp = (store) => {
    return {
        article: store.articleR
    }
}

export default connect(mTp, { GetArticle, GetAuthor, DeleteArticle })(Home)
