import React, { Component } from 'react'
import { Image, ImageBackground, SafeAreaView, Text, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { convertDate } from '../../function/function'
import { color } from '../../theme/Color'
import { font } from '../../theme/Font'


export class Home_Detail extends Component {

        state = {
            image : 'https://www.kinmade.com/pub/media/catalog/product/placeholder/default/no-image-available_1.jpg'
        }

    componentDidMount = async () => {

        await this.props.navigation.setOptions({
            title: "Article"
        })

    }

    render() {
        const { item } = this.props.route.params
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <ImageBackground style={{ width: "100%", height: 55, flex: 1 }} source={require('../../images/my_back.png')}>
                    <View style={{ flex: 1, alignItems: "flex-end", marginRight: 15, marginTop: 5 }}>
                        <Text style={{ fontFamily: "Billabong", fontSize: 45 }}>Detail</Text>
                    </View>
                </ImageBackground>
                <View style={{ flex: 10, padding: 10 }}>
                    <View >
                        <Image source={{ uri: item.image === '' ? this.state.image : item.image }} style={{ height: 300, width: '100%', resizeMode: 'contain', borderRadius: 5 }} >
                        </Image>
                    </View>
                    <View style={{  padding: 5 }}>
                        <View>
                            <Text style={{ fontSize: font.size_header, fontWeight: "bold", color: color.danger }}>{item.title}</Text>
                        </View>
                        <View style={{ marginTop: 5 }}>
                            <Text style={{ fontSize: font.size_title, fontWeight: "bold" }}>{"Category​ ៖ "}{item.category && item.category.name}</Text>
                        </View>
                        <View style={{ marginTop: 5 }}>
                            <Text style={{ fontSize: font.size_title, fontWeight: "bold", color: "gray" }}>{convertDate(item.createdAt)}</Text>
                        </View>
                    </View>
                    <View style={{padding:5,flex:1}}>
                        <ScrollView >
                            <Text style={{ marginBottom: 20, fontSize: font.size_title, fontWeight: "bold", color: "gray" }}>{item.description}</Text>
                        </ScrollView>
                    </View>
                </View>

            </SafeAreaView>
        )
    }
}

export default Home_Detail
