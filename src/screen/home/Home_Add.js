
import { Button, Card, Form, Icon, Input, Item, Textarea } from 'native-base'
import React, { Component } from 'react'
import { Image, ImageBackground, Keyboard, KeyboardAvoidingView, SafeAreaView, Text, TouchableOpacity, View } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { style_in } from '../authentication/Login'
import { launchImageLibrary } from 'react-native-image-picker';
import { font } from '../../theme/Font'
import { PostImage, PostArticle ,EditArticle } from "../../redux/action/Article_Action";
import { connect } from 'react-redux'
import DailogWaiting from '../../components/DailogWaiting'
class Home_Add extends Component {

    state = {
        avatarSource: null,
        id: '',
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/Icons8_flat_add_image.svg/512px-Icons8_flat_add_image.svg.png',
        file: '',
        title: '',
        description: '',
        isLoading: false,
        label: "Add"
    }

    componentDidUpdate = async (prevProp, prevState) => {
        if (this.props.route.params.item !== null && (this.props.route.params.item !== prevProp.route.params.item)) {
            const { item } = this.props.route.params;
            this.setState({
                id: item._id,
                image: item.image,
                file: '',
                title: item.title,
                description: item.description,
                label: "Edit"
            })
        }
    }

    componentDidMount = async () => {

        await this.props.navigation.setOptions({
            title: "Article"
        })

        if (this.props.route.params.item !== null) {
            const { item } = this.props.route.params;
            this.setState({
                id: item._id,
                image: item.image,
                file: '',
                title: item.title,
                description: item.description,
                label: "Edit"
            })
        }

    }

    onChooseImg = () => {

        let body = new FormData();

        launchImageLibrary({
            mediaType: 'photo',
            quality: 0.5
        },
            (response) => {
                if (!response.didCancel && !response.error) {

                    body.append('image', { type: response.type, uri: response.uri, name: response.type.substring(4, response.type.length) })

                    this.setState({
                        avatarSource: response.uri,
                        image: '',
                        file: body
                    })
                }
            })
    }


    onSubmit = async () => {

        this.setState({
            isLoading: true
        })

        if (this.state.id === '') {
            // insert
            await this.props.PostImage(this.state.file);
            var obj = {
                id: this.state.id,
                title: this.state.title,
                description: this.state.description,
                published: true,
                image: this.props.article.image.url
            }
            await this.props.PostArticle(obj);
        } 
        else {
            // update
            if(this.state.image === ''){
                await this.props.PostImage(this.state.file);
            }
          
            var obj = {
                id: this.state.id,
                title: this.state.title,
                description: this.state.description,
                published: true,
                image: this.state.image === '' ? this.props.article.image.url : this.state.image
            }

            await this.props.EditArticle(obj);         
        }

        this.setState({
            id: '',
            avatarSource: null,
            image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/Icons8_flat_add_image.svg/512px-Icons8_flat_add_image.svg.png',
            file: '',
            title: '',
            description: '',
            isLoading: false,
            label: "Add"         
        })
        
        this.props.navigation.navigate('Home');
       
    }

    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <DailogWaiting visible={this.state.isLoading}> </DailogWaiting>
                <View >
                    <ImageBackground style={{ width: "100%", height: 55 }} source={require('../../images/my_back.png')}>
                        <View style={{ flex: 1, alignItems: "flex-end", marginRight: 15, marginTop: 5 }}>
                            <Text style={{ fontFamily: "Billabong", fontSize: 45 }}>{this.state.label}</Text>
                        </View>
                    </ImageBackground>
                </View>
                <KeyboardAwareScrollView style={{ padding: 20 }}>
                    <View style={{ marginBottom: 20 }}>
                        <TouchableOpacity activeOpacity={0.8} onPress={() => this.onChooseImg()}>
                            <Image source={{ uri: this.state.avatarSource === null ? this.state.image : this.state.avatarSource }} style={{ height: 300, width: '100%', resizeMode: 'contain', borderRadius: 5 }} >
                            </Image>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginBottom: 20 }}>
                        {this.state.title === '' ? null : <Text style={{ margin: 5 }}>Title</Text>}
                        <Item regular style={{ borderRadius: 5 }}>
                            <Input placeholder='Title' value={this.state.title} onChangeText={(txt) => this.setState({ title: txt })} />
                        </Item>
                    </View>
                    <View style={{ marginBottom: 20 }}>
                        {this.state.description === '' ? null : <Text style={{ margin: 5 }}>Description</Text>}
                        <Form  >
                            <Textarea rowSpan={7} bordered placeholder="Description" style={{ borderRadius: 5 }} value={this.state.description} onChangeText={(txt) => this.setState({ description: txt })} />
                        </Form>
                    </View>
                </KeyboardAwareScrollView>
                <View style={{ padding: 20 }}>
                    <Button iconLeft full danger onPress={() => this.onSubmit()} style={{ borderRadius: 5 }}>
                        <View style={{ width: 35 }}>
                            <Icon type="FontAwesome" name="save" />
                        </View>
                        <View style={{ width: 70 }}>
                            <Text style={style_in.font_title}>Save</Text>
                        </View>
                    </Button>
                </View>
            </SafeAreaView>
        )
    }
}

const mTp = (store) => {
    return {
        article: store.articleR
    }
}

export default connect(mTp, { PostImage, PostArticle,EditArticle })(Home_Add)
