
import React, { Component } from 'react'
import { Image, ImageBackground, StyleSheet, Text, View } from 'react-native'
import { style } from '../../theme/Style'
import { Button, Icon, Input, Thumbnail } from 'native-base'
import { color } from '../../theme/Color'
import { font } from '../../theme/Font'
import { LoginButton, AccessToken, GraphRequest, GraphRequestManager, LoginManager } from 'react-native-fbsdk';
import { OnLogin} from '../../redux/action/Article_Action'
import { connect } from 'react-redux'
export class Login extends Component {
 
    state = {
        userInfo: {},
        picture: {}
    };
    getInfoFromToken = token => {
        const PROFILE_REQUEST_PARAMS = {
            fields: {
                string: 'id, name,  first_name, last_name,picture.type(large)',
            },
        };
        const profileRequest = new GraphRequest(
            '/me',
            { token, parameters: PROFILE_REQUEST_PARAMS },
            (error, result) => {
                if (error) {
                    console.log('login info has error: ' + error);
                } else {
                    this.setState({ userInfo: result ,picture :  result.picture.data });
                    this.props.OnLogin({user: result,pic: result.picture.data})
                    this.props.navigation.navigate('Home'); 
                }
            },
        );
        new GraphRequestManager().addRequest(profileRequest).start();
    };

    onFacebook =  () => {       
        LoginManager.logInWithPermissions(["public_profile"]).then(
            (result) => {
              if (result.isCancelled) {
                console.log("Login cancelled");
              } else {  
                AccessToken.getCurrentAccessToken().then(
                    (data) => {
                        const accessToken = data.accessToken.toString();
                        this.getInfoFromToken(accessToken);                     
                    }
                )            
              }
            },
            function(error) {
              console.log("Login fail with error: " + error);
            }
          );
         
    }
    render() {
        return (
            <View style={style.container}>
                <View style={style_in.header_login}>
                </View>
                <ImageBackground style={style_in.img_background} source={require('../../images/my_cover_login.png')}>
                    <View style={style_in.img_header}>
                        <Thumbnail style={style_in.Thumbnail_Style} source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }} />
                        <Text style={style_in.font_header}>Welcome !</Text>
                        <Text style={style_in.font_title}>Sign In to continue</Text>
                    </View>
                </ImageBackground>
                <View style={style_in.body}>
                    <View style={{ padding: 5 }}>
                        <Button iconLeft full danger onPress={() => this.onFacebook()} style={{ borderRadius: 5 }}>
                            <View style={{ width: 35 }}>
                                <Icon type="FontAwesome" name="facebook" />
                            </View>
                            <View style={{ width: 150 }}>
                                <Text style={style_in.font_title}>Facebook</Text>
                            </View>
                        </Button>
                    </View>
                    <View style={{ padding: 5 }}>
                        <Button iconLeft full danger style={{ borderRadius: 5 }} >
                            <View style={{ width: 35 }}>
                                <Icon type="FontAwesome" name="google" />
                            </View>
                            <View style={{ width: 150 }}>
                                <Text style={style_in.font_title}>Google Gmail</Text>
                            </View>
                        </Button>
                    </View>
                    <View style={{ padding: 5 }}>
                        <Button iconLeft full danger style={{ borderRadius: 5 }}>
                            <View style={{ width: 35 }}>
                                <Icon type="FontAwesome" name="phone" />
                            </View>
                            <View style={{ width: 150 }}>
                                <Text style={style_in.font_title}>Phone Number</Text>
                            </View>
                        </Button>
                    </View>                                   
                </View>      
                <View style={style_in.footer}>
                    <Text style={style_in.font_title}>Power by @Sagi Dev</Text>
                </View>

            </View>
        )
    }
}

const mTp = (store) => {
    return {
        article: store.articleR
    }
}

export default connect(mTp, { OnLogin})(Login)

export const style_in = StyleSheet.create({
    header_login: {
        backgroundColor: color.danger, width: "100%", height: 150
    },
    img_background: {
        width: "100%", height: 120
    },
    img_header: {
        justifyContent: "center", alignItems: "center", marginTop: -70
    },
    Thumbnail_Style: {
        borderColor: color.white, borderWidth: 2
    },
    font_header: {
        color: color.white, fontSize: font.size_header
    },
    font_title: {
        color: color.white, fontSize: font.size_title, textAlign: "left", marginLeft: 10
    },
    body: {
        backgroundColor: color.white, width: "100%", flex: 1, padding: 15
    },
    footer: {
        backgroundColor: color.danger, width: "100%", height: 45, justifyContent: "center", alignItems: "center",
    }
})