

import React, { Component } from 'react'
import { ActivityIndicator, Modal, StyleSheet, Text, View } from "react-native"
import { color } from '../theme/Color'




export class DailogWaiting extends Component {

    render() {
        return (
            <View>    
                <Modal
                    animationType="fade"   
                    transparent={true}               
                    visible={this.props.visible}>
                   
                   <View style={{flex:1,justifyContent:"center", backgroundColor:'rgba(0,0,0,0.5)'}}>                                              
                        <View style={style_in.style_modal}>
                            <View style={{ marginLeft: 20 }}>
                                <ActivityIndicator size="large" color={color.danger} />
                            </View>
                            <View style={{ marginLeft: 20 }}>
                                <Text>Loading............</Text>
                            </View>
                        </View>
                    </View>      
                </Modal>
            </View>
        )
    }
}

export default DailogWaiting


const style_in = StyleSheet.create({
    style_modal: {      
        justifyContent:"flex-start", 
         margin: 20,
        backgroundColor: "white",
        flexDirection: "row",
        borderRadius: 5,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 15
    },
})