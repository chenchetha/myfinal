export const Action_Type = {
    GetArticle: 'GetArticle',
    GetAuthor: 'GetAuthor',
    PostImage: 'PostImage',
    PostArticle: 'PostArticle',
    EditArticle: 'EditArticle',
    DeleteArticle: 'DeleteArticle',
    Login: 'Login'
}