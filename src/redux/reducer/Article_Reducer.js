import { Action_Type } from "../Action_Type"

const initState = {
    lst_article: [],
    lst_author: [],
    image: '',
    article: '',
    isUpdate: false,
    user: {}
}



export const Article_Reducer = (state = initState, action) => {
    switch (action.type) {
        case Action_Type.GetArticle:
            return {
                ...state, lst_article: action.payload
            }
        case Action_Type.GetAuthor:
            return {
                ...state, lst_author: action.payload
            }
        case Action_Type.PostImage:
            return {
                ...state, image: action.payload
            }
        case Action_Type.PostArticle:
            return {
                ...state, article: action.payload, isUpdate: true
            }
        case Action_Type.EditArticle:
            return {
                ...state, article: action.payload, isUpdate: true
            }
        case Action_Type.DeleteArticle:
            return {
                ...state, article: action.payload, isUpdate: true
            }
        case Action_Type.Login:
            return {
                ...state, user: action.payload 
            }
        default:
            return state
    }
}