
import axios from "../../API"
import { Action_Type } from "../Action_Type"


export const GetArticle = (data) => {
    header = {
        headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json'
        }
    }
    return (dispatch) => {
        return axios.get(`/api/articles?title=${data.search}&page=${data.page}&size=${data.limit}`, header)
            .then(res =>
                dispatch({
                    type: Action_Type.GetArticle,
                    payload: res.data
                })
            )
            .catch(err => alert(err))
    }
}


export const GetAuthor = (data) => {
    header = {
        headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json'
        }
    }
    return (dispatch) => {
        return axios.get(`/api/author`, header)
            .then(res =>
                dispatch({
                    type: Action_Type.GetAuthor,
                    payload: res.data
                })
            )
            .catch(err => alert(err))
    }
}


export const PostImage = (data) => {
    header = {
        headers: {
            'Content-type': 'multipart/form-data',
            'Accept': 'application/json'
        }
    }
    return (dispatch) => {
        return axios.post(`/api/images`, data, header)
            .then(res =>
                dispatch({
                    type: Action_Type.PostImage,
                    payload: res.data
                })
            )
            .catch(err => alert(err))
    }
}


export const PostArticle = (data) => {
    header = {
        headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json'
        }
    }
    return (dispatch) => {
        return axios.post(`/api/articles`, data, header)
            .then(res =>
                dispatch({
                    type: Action_Type.PostArticle,
                    payload: res.data
                })
            )
            .catch(err => alert(err))
    }
}



export const EditArticle = (data) => {

    header = {
        headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json'
        }
    }

    var obj = {
        title: data.title,
        description: data.description,
        published: true,
        image: data.image,
    }

    return (dispatch) => {
        return axios.patch(`/api/articles/${data.id}`, obj, header)
            .then(res =>
                dispatch({
                    type: Action_Type.EditArticle,
                    payload: res.data
                })
            )
            .catch(err => alert(err))
    }
}


export const DeleteArticle = (id) => {

    header = {
        headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json'
        }
    }

    return (dispatch) => {
        return axios.delete(`/api/articles/${id}`,header)
            .then(res =>
                dispatch({
                    type: Action_Type.DeleteArticle,
                    payload: res.data
                })
            )
            .catch(err => alert(err))
    }
}

export const OnLogin = (data) => {
    return (dispatch) => {     
                dispatch({
                    type: Action_Type.Login,
                    payload: data
                })    
    }
}


