import React, { Component } from 'react'
import { SafeAreaView, StyleSheet, Text, View } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import { Button, Card, Icon, Thumbnail } from 'native-base';
import Home from '../screen/home/Home';
import Login, { style_in } from '../screen/authentication/Login';
import { Easing } from 'react-native-reanimated';
import { DrawerActions, NavigationContainer } from '@react-navigation/native';
import { color } from '../theme/Color';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { font } from '../theme/Font';
import Profile from '../screen/tab/Profile';
import Home_Detail from '../screen/home/Home_Detail';
import Home_Add from '../screen/home/Home_Add';
import Author from '../screen/home/Author';
import { connect } from 'react-redux';
import { LoginManager } from 'react-native-fbsdk';


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const config = {
    animation: 'spring',
    config: {
        stiffness: 1000,
        damping: 500,
        mass: 3,
        overshootClamping: false,
        restDisplacementThreshold: 0.01,
        restSpeedThreshold: 0.01
    }
}

const closeConfig = {
    animation: "timing",
    config: {
        duration: 500,
        easing: Easing.linear
    }
}

function AllScreen(props) {

    return (
        <Stack.Navigator
            screenOptions={{
                gestureEnabled: true,
                gestureDirection: "horizontal",
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                transitionSpec: {
                    open: config,
                    close: closeConfig
                }
            }}
        >
            <Stack.Screen
                name="Login"
                component={Login}
                options={{
                    headerShown: false
                }}
            />

            <Stack.Screen
                name="Home"
                children={() => <MyDrawer {...props} />}
                options={{
                    headerShown: false,
                    headerBackTitle: " ",
                }}
            />

            <Stack.Screen
                name="Home_Detail"
                component={Home_Detail}
                options={{
                    headerBackTitle: " ",
                    headerStyle: {
                        backgroundColor: color.danger,
                        elevation: 0,
                        shadowOpacity: 0,
                        borderBottomWidth: 0,
                    },
                    headerTintColor: "white",
                }}
            />

            <Stack.Screen
                name="Home_Add"
                component={Home_Add}
                options={{
                    headerBackTitle: " ",
                    headerStyle: {
                        backgroundColor: color.danger,
                        elevation: 0,
                        shadowOpacity: 0,
                        borderBottomWidth: 0,
                    },
                    headerTintColor: "white",
                }}
            />

            <Stack.Screen
                name="Author"
                component={Author}
                options={{
                    headerBackTitle: " ",
                    headerStyle: {
                        backgroundColor: color.danger,
                        elevation: 0,
                        shadowOpacity: 0,
                        borderBottomWidth: 0,
                    },
                    headerTintColor: "white",
                }}
            />

        </Stack.Navigator>
    );
}

function MyHeaderLeft(navigation) {
    return (
        <TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}>
            <Icon type="MaterialIcons" name="reorder" style={style_nav.icon} />
        </TouchableOpacity>
    )
}

function MyHraderRight(navigation, props) {
    return (
        <TouchableOpacity>
            <Icon type="MaterialIcons" name="more-vert" style={style_nav.icon} />
        </TouchableOpacity>
    )
}

const DrawerOptions = (nameIcon, title, showHeader, navigation, props) => {
    return (
        {
            drawerLabel: ({ focused }) => (<Text style={{ fontSize: 15, color: focused === true ? color.danger : 'gray' }}>{title}
            </Text>),
            labelStyle: { marginLeft: 50 },
            drawerIcon: ({ focused }) => (
                <View>
                    <Icon type="MaterialIcons" name={nameIcon} style={{ color: focused === true ? color.danger : 'gray' }} />
                </View>
            ),
            headerShown: showHeader,
            title: title,
            headerLeft: () => MyHeaderLeft(navigation),
            headerRight: () => MyHraderRight(navigation, props),
            headerStyle: {
                backgroundColor: color.danger,
                elevation: 0,
                shadowOpacity: 0,
                borderBottomWidth: 0,
            },
            headerTitleStyle: { fontSize: 17 },
            headerTintColor: "white",

        }

    )
}

const DrawerContentOptions = () => {
    return (
        {
            activeBackgroundColor: 'whitesmoke',
            itemStyle: { height: 50, marginTop: -2, marginLeft: 5, marginRight: 5, borderRadius: 5 },

        }
    )
}

function MyDrawer(props) {
    return (
        <Drawer.Navigator drawerContent={props_ => CustomDrawerContent(props_, props)} drawerContentOptions={DrawerContentOptions()}>
            <Drawer.Screen name="Home" component={MyTab} options={({ navigation }) => DrawerOptions('home', "Home", true, navigation, props)} />
            <Drawer.Screen name="Home_Add" initialParams={{ item: null }} component={Home_Add} options={({ navigation }) => DrawerOptions('add-box', "AddNew Article", true, navigation, props)} />
        </Drawer.Navigator>
    );
}

function logout(props){
    LoginManager.logOut();
    props.navigation.navigate('Login');
}


function CustomDrawerContent(props, passProps) {
    return (

        <SafeAreaView style={{ flex: 1 }}>
            <Card>
                <View style={{ width: '100%', marginTop: 5, flexDirection: 'column', marginBottom: 10 ,justifyContent: "center", alignItems: "center"}}>
                    <Thumbnail large source={{ uri: passProps.article.user.pic.url }} style={{ margin: 2 }} />                   
                    <View style={{ marginLeft: 10, alignItems: "flex-start" }}>
                        <Text style={{ marginTop: 5, textAlign: 'center', fontSize: font.size_header,fontWeight: "bold"}}>{passProps.article.user.user.name}</Text>
                    </View>
                </View>
            </Card>

            <DrawerContentScrollView {...props}>
                <DrawerItemList {...props} />
            </DrawerContentScrollView>

            <View >
                <Button iconLeft full danger onPress={()=>logout(props)}>
                    <View >
                        <Icon type="MaterialIcons" name="keyboard-backspace" style={style_nav.icon} />
                    </View>
                    <View style={{ width: 70 }}>
                        <Text style={style_in.font_title}>Logout</Text>
                    </View>
                </Button>
            </View>

        </SafeAreaView>
    );

}

const TabbarOption = () => {
    return (
        {
            style: {
                backgroundColor: 'white',
                height: Platform.OS === 'ios' ? 65 : 75
            },
        }
    )
}

const TabOption = (nameIcon, title) => {
    return (
        {
            tabBarLabel: ({ focused }) => <Text style={{ marginBottom: Platform.OS === 'ios' ? 0 : 15, fontSize: focused === true ? 17 : 15, color: focused === true ? color.danger : 'gray' }}>{title}</Text>,
            tabBarIcon: ({ focused }) => <Icon type="MaterialIcons" name={nameIcon} style={{ color: focused == true ? color.danger : "gray", fontSize: 30 }} />
        }
    );
}

function MyTab() {
    return (
        <Tab.Navigator tabBarOptions={TabbarOption()} >
            <Tab.Screen name="home" component={Home} tabBarOptions={TabbarOption} options={TabOption('import-contacts', 'Article')} />
            <Tab.Screen name="profile" component={Profile} tabBarOptions={TabbarOption} options={TabOption('person-outline', 'Profile')} />
        </Tab.Navigator>
    );
}


export class AppNavigator extends Component {
    render() {
        return (
            <NavigationContainer  >
                <AllScreen {...this.props} >  </AllScreen>
            </NavigationContainer>
        )
    }
}


const mTp = (store) => {
    return {
        article: store.articleR
    }
}

export default connect(mTp,)(AppNavigator)

export const style_nav = StyleSheet.create({
    icon: {
        color: color.white, margin: 20, fontSize: font.size_header
    }
})